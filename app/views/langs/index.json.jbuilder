json.array!(@langs) do |lang|
  json.extract! lang, :id, :name, :level_id
  json.url lang_url(lang, format: :json)
end
